import {Endereco} from './endereco';

export class Condutor {
  cpf: string;
  nome: string;
  rg: string;
  profissao: string;
  endereco: Endereco;
}
