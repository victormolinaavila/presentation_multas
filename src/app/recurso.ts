import {Veiculo} from './veiculo';
import {Proprietario} from './proprietario';
import {Condutor} from './condutor';

export class Recurso {
  veiculo: Veiculo;
  proprietario: Proprietario;
  condutor: Condutor;
  penalidade: string;
  orgaoAutuante: string;
  numeroAutuacao: string;
  artigo: string;
  dataInfracao: string;
  horaInfracao: string;
  foiAbordado: string;
  assinouOAutoDeInfracao: string;
  contemRasura: string;
  campoRasurado: string;
  letraLegivel: string;
}
