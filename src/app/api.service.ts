import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Recurso } from './recurso';
import {Veiculo} from './veiculo';
import {Proprietario} from './proprietario';
import {Endereco} from './endereco';
import {Condutor} from './condutor';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = "http://localhost:8080/recurso";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  addProduct(form): Observable<Recurso> {
    const recurso = new Recurso();
    const veiculo = new Veiculo();
    recurso.veiculo = veiculo;
    recurso.veiculo.marca = form.marca;
    recurso.veiculo.modelo = form.modelo;
    recurso.veiculo.placa = form.placa;
    recurso.veiculo.renavam = form.renavam;

    const proprietario = new Proprietario();
    const enderecoProprietario = new Endereco();
    recurso.proprietario = proprietario;
    recurso.proprietario.cpf = form.cpfProprietario;
    recurso.proprietario.nome = form.nomeProprietario;
    recurso.proprietario.rg = form.rgProprietario;
    recurso.proprietario.profissao = form.profissaoProprietario;
    recurso.proprietario.endereco = enderecoProprietario;
    recurso.proprietario.endereco.cep = form.cepProprietario;
    recurso.proprietario.endereco.estado = form.estadoProprietario;
    recurso.proprietario.endereco.cidade = form.cidadeProprietario;
    recurso.proprietario.endereco.bairro = form.bairroProprietario;
    recurso.proprietario.endereco.rua = form.ruaProprietario;
    recurso.proprietario.endereco.numero = form.numeroProprietario;
    console.log(form.cpfCondutor);
    const condutor = new Condutor();
    const enderecoCondutor = new Endereco();
    recurso.condutor = condutor;
    recurso.condutor.cpf = form.cpfCondutor;
    recurso.condutor.nome = form.nomeCondutor;
    recurso.condutor.rg = form.rgCondutor;
    recurso.condutor.profissao = form.profissaoCondutor;
    recurso.condutor.endereco = enderecoCondutor;
    recurso.condutor.endereco.cep = form.cepCondutor;
    recurso.condutor.endereco.estado = form.estadoCondutor;
    recurso.condutor.endereco.cidade = form.cidadeCondutor;
    recurso.condutor.endereco.bairro = form.bairroCondutor;
    recurso.condutor.endereco.rua = form.ruaCondutor;
    recurso.condutor.endereco.numero = form.numeroCondutor;

    recurso.penalidade = form.penalidade;
    recurso.orgaoAutuante = form.orgaoAutuante;
    recurso.numeroAutuacao = form.numeroAutuacao;
    recurso.artigo = form.artigo;
    recurso.dataInfracao = form.dataInfracao;
    recurso.horaInfracao = form.horaInfracao;
    recurso.foiAbordado = form.foiAbordado;
    recurso.assinouOAutoDeInfracao = form.assinouOAutoDeInfracao;
    recurso.contemRasura = form.contemRasura;
    recurso.campoRasurado = form.campoRasurado;
    recurso.letraLegivel = form.letraLegivel;
    return this.http.post<Recurso>(apiUrl, recurso, httpOptions).pipe(
      tap(( teste: Recurso ) => console.log('recurso criado com sucesso')),
      catchError(this.handleError<Recurso>('addRecurso'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
