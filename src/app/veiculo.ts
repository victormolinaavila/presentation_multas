export class Veiculo {
  marca: string;
  modelo: string;
  placa: string;
  renavam: string;
}
