import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  productForm: FormGroup;
  penalidades: string[] = ['MULTA DE TRÂNSITO', 'SUSPENSÃO DE CNH', 'CASSAÇÃO DE CNH'];
  respostas: string[] = ['Sim', 'Não'];
  meessage: boolean = false;

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      'marca' : [null, Validators.required],
      'modelo' : [null, Validators.required],
      'placa' : [null, Validators.required],
      'renavam' : [null, Validators.required],
      'cpfProprietario' : [null, Validators.required],
      'nomeProprietario' : [null, Validators.required],
      'rgProprietario' : [null, Validators.required],
      'profissaoProprietario' : [null, Validators.required],
      'cepProprietario' : [null, Validators.required],
      'estadoProprietario' : [null, Validators.required],
      'cidadeProprietario' : [null, Validators.required],
      'bairroProprietario' : [null, Validators.required],
      'ruaProprietario' : [null, Validators.required],
      'numeroProprietario' : [null, Validators.required],
      'cpfCondutor' : [null, Validators.required],
      'nomeCondutor' : [null, Validators.required],
      'rgCondutor' : [null, Validators.required],
      'profissaoCondutor' : [null, Validators.required],
      'cepCondutor' : [null, Validators.required],
      'estadoCondutor' : [null, Validators.required],
      'cidadeCondutor' : [null, Validators.required],
      'bairroCondutor' : [null, Validators.required],
      'ruaCondutor' : [null, Validators.required],
      'numeroCondutor' : [null, Validators.required],
      'penalidade' : [null, Validators.required],
      'orgaoAutuante' : [null, Validators.required],
      'numeroAutuacao' : [null, Validators.required],
      'artigo' : [null, Validators.required],
      'dataInfracao' : [null, Validators.required],
      'horaInfracao' : [null, Validators.required],
      'foiAbordado' : [null, Validators.required],
      'assinouOAutoDeInfracao' : [null, Validators.required],
      'contemRasura' : [null, Validators.required],
      'campoRasurado' : [null, Validators.required],
      'letraLegivel' : [null, Validators.required],
      'meessage' : [null]
    });
  }

  onFormSubmit(form: NgForm ) {
    this.api.addProduct(form)
      .subscribe(res => {
      }, (err) => {
        console.log(err);
      });
  }

  // handleClick(event: Event) {
  //   test =1;
  // }
}
