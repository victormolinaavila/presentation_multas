import {Endereco} from './endereco';

export class Proprietario {
  cpf: string;
  nome: string;
  rg: string;
  profissao: string;
  endereco: Endereco;
}
